import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { NoticiasService } from "../../domain/noticias.service";
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: "detail",
    templateUrl: "./detail.component.html"
})
export class DetailComponent implements OnInit {
    description: String;
    constructor(private route: ActivatedRoute, private noticias: NoticiasService) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        const id = this.route.snapshot.paramMap.get('id');
        let noticia = this.noticias.buscar();
        this.description = noticia[id];
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
